<?php

use App\Http\Controllers\Authentication\AuthController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WaiterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/cadastro', [UserController::class, 'store'])->name('cadastro');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/users', [UserController::class, 'index']);
    Route::get('/user', [UserController::class, 'show']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/roles', [RolesController::class, 'index']);
    Route::get('/role', [RolesController::class, 'show']);
    Route::put('/user/{id}', [UserController::class, 'update']);
    Route::delete('/user/{id}', [UserController::class, 'destroy']);
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/products', [ProductController::class, 'store']);
    Route::get('/products', [ProductController::class, 'index']);
    Route::get('/product/{id}', [ProductController::class, 'show']);
    Route::put('/product/{id}', [ProductController::class, 'update']);
    Route::delete('/product/{id}', [ProductController::class, 'destroy']);
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/waiter', [WaiterController::class, 'store']);
    Route::get('/waiters', [WaiterController::class, 'index']);
    Route::get('/waiter/{id}', [WaiterController::class, 'show']);
    Route::put('/waiter/{id}', [WaiterController::class, 'update']);
    Route::delete('/waiter/{id}', [WaiterController::class, 'destroy']);
});


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/customer', [CustomersController::class, 'store']);
    Route::get('/customers', [CustomersController::class, 'index']);
    Route::get('/customer/{id}', [CustomersController::class, 'show']);
    Route::put('/customer/{id}', [CustomersController::class, 'update']);
    Route::delete('/customer/{id}', [CustomersController::class, 'destroy']);
});

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/sale', [SalesController::class, 'store']);
    Route::get('/sales', [SalesController::class, 'index']);
    Route::get('/sale/{id}', [SalesController::class, 'show']);
    Route::put('/sale/{id}', [SalesController::class, 'update']);
});