## DOCUMENTO DE VISÃO

### Histórico de Versões
| Data  | Versao | Autor  | Revisor | Aprovador por
| ------------ | ------------ | ------------ | ------------ | ------------ |
| 12/03/2021 | 1.0 | Niely Patrícia  | Débora Aparecida Brito Cavalcante | Renan Maia |

## RESPONSÁVEIS
1.  Equipe Débora e Niely
Coordenador
Desenvolvimento
2. Cliente
  Gestor do Sistema

### 1. Objetivo
O propósito deste documento é coletar, analisar e definir as necessidades de alto-nível e características do projeto de software, focando nas potencialidades requeridas pelos afetados e usuários-alvo, e como estes requisitos serão abordados no projeto de software.
A visão do projeto documenta o ambiente geral de processos a ser desenvolvido para o sistema durante o projeto, fornecendo a todos os envolvidos uma descrição compreensível deste e de suas macro-funcionalidades.
O Documento de Visão de Projeto documenta apenas as necessidades e funcionalidades do sistema que estão sendo atendidas no projeto de software.

### 2. Descrição do projeto
Esse projeto consiste no desenvolvimento de uma aplicação móvel de controle de um caixa de um bar, que precisa registar os pedidos, contar a quantidade de gorjetas dos garçons. O aplicativo visa auxiliar na prestação de serviços de um bar.

#####2.1. Papel dos atores
| Descrição  | Desenvolvedores dos projetos |
| ------------ | ------------ |
| **Papel** | Projetar a aplicação, documentar a estrutura do software, programar os recursos definidos e realizar testes de implementação.  |
| **Insumos ao sistema ** | Planejar, desenvolver e implementar o sistema.  |
| **Representante** | Aplicativo |

| Descrição  | Clientes |
| ------------ | ------------ |
| **Papel** |   |
| **Insumos ao sistema ** |   |
| **Representante** | Aplicativo  |

| Descrição  | Administrador |
| ------------ | ------------ | 
| **Papel** |  Figura monitoramento |
| **Insumos ao sistema ** | O administrador exerce o papel de cadastrar, editar, excluir os dados necessários ao sistema |
| **Representante** | Aplicativo  |

| Descrição  | Garçom |
| ------------ | ------------ |
| **Papel** |  Usuário pede a consulta de dados |
| **Insumos ao sistema ** | Permitir o acesso aos dados cadastrados, cadastra vendas no sistema  |
| **Representante** | Aplicativo |

### 3. Necessidades e Funcionalidades
|  Necessidade 1 | Benefício  |
| ------------ | ------------ | 
|  Cadastrar Dados | < Crítico >  |
| Id. Funcionalidades  |  Descrição das funcionalidades/atores envolvidos |
| **F1.1**  | Cadastrar abertura e fechamento de caixa, funcionários, vendas, clientes. / **administrador** |
| **F1.2**  | Editar funcionários, vendas, clientes.  / **administrador**  |
| **F1.3**  | Visualizar funcionários, vendas, clientes.  / **administrador**  |
| **F1.4**  | Listar funcionários, vendas, clientes.  / **administrador** |
| **F1.5**  | Visualizar funcionário, venda, cliente.  / **administrador** |

|  Necessidade 2 | Benefício|
| ------------ | ------------ |
| Cadastrar Dados | < Crítico >  |
| Id. Funcionalidades  |  Descrição das funcionalidades/atores envolvidos |
| **F2.1**  | Inserir os pedidos no sistema . / **usuário - garçom** |
| **F2.2**  | Lista todos os faturamentos de vendas e valor de gorjetas .  / **usuário - garçom** |
| **F1.3**  | Visualizar vendas, produtos e clientes.  / **usuário - garçom**  |

### 4. Premissas e restrições
| Premissas  | Restrições |
| ------------ | ------------ |
| - O sistema importará dados do bar cadastrado para se manter atualizado. <br> - O sistema contará com um número limitado de servidores. <br> - O sistema funcionará 24 horas por dia  durante os 7 dias da semana. <br>-  O usuário deve estar cadastrado no sistema | - O ambiente da empresa sofre manutenção aos finais de semana, esses dias não poderão ser considerados no cronograma. <br> - O sistema não deve enviar e-mail. <br> - O sistema funcionará com dados móveis.|