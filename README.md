## Sobre o projeto

Este projeto visa criar uma API para ser consumida em um sistema mobile

## Requisitos
- PostgreSQL
- Apache2
- php >7.2
- Modulo PostgreSQL para php
- Habilitar mod_rewrite: a2enmod rewrite / service apache2 restart
- Composer
## Como rodar esse projeto
$ git clone https://gitlab.com/debora.brito/computacao-movel.git <br>
$ cd computacao-movel <br>
$ composer install <br>
$ cp .env.example .env <br>
$ php artisan key:generate <br>
$ php artisan migrate #antes de rodar este comando verifique sua configuracao com banco em .env <br>
$ php artisan serve <br>
$ php artisan db:seed #para gerar os seeders, dados de teste <br>

- Acessar pela url gerada com o comando de php artisan serve

#Documentos
O modelo de dados e os Requisitos do sistema estão na pasta documentos
