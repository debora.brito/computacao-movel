<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('users')->insertOrIgnore(
            [
                [
                    'name' => 'Debora',
                    'email' => 'admin@gmail.com',
                    'password' => bcrypt('admin'),
                    'email_verified_at' => Carbon::now(),
                    'role_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
            ]
        );
    }
}
