<?php

namespace App\Exceptions\User;

use Exception;

class UserNotFoundException extends Exception
{
    public function report()
    {
        return true;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Usuário não encontrado.']]], 404);
    }
}
