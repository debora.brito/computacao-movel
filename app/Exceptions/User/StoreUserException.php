<?php

namespace App\Exceptions\User;

use Exception;

class StoreUserException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        return true;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Falha ao salvar dados do usuário.']]], 500);
    }
}
