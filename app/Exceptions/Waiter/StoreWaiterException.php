<?php

namespace App\Exceptions\Waiter;

use Exception;

class StoreWaiterException extends Exception
{
    public function report()
    {
        return true;
    }

    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Falha ao salvar garçom.']]], 500);
    }
}
