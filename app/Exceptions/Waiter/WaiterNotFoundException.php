<?php

namespace App\Exceptions\Waiter;

use Exception;

class WaiterNotFoundException extends Exception
{
    public function report()
    {
        return true;
    }

    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Garçom não encontrado.']]], 404);
    }
}
