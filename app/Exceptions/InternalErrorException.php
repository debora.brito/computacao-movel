<?php

namespace App\Exceptions;

use Exception;

class InternalErrorException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        return true;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Erro do servidor.']]], 500);
    }
}
