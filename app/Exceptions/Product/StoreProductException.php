<?php

namespace App\Exceptions\Product;

use Exception;

class StoreProductException extends Exception
{
    public function report()
    {
        return true;
    }

    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Falha ao salvar o produto.']]], 500);
    }
}
