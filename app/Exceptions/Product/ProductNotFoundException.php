<?php

namespace App\Exceptions\Product;

use Exception;

class ProductNotFoundException extends Exception
{
    public function report()
    {
        return true;
    }

    public function render($request)
    {
        return response()->json(['Errors' => [['server' => 'Produto não encontrado.']]], 404);
    }
}
