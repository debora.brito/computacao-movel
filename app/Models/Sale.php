<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = ['total'];

    public function waiter()
    {
        return $this->belongsTo(Waiter::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
