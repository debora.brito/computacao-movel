<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string|email',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'email.email' => 'Insira um e-mail válido',
            'email.required' => 'O e-mail é obrigatório',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = [
            'Errors' => [$validator->errors()]
        ];

        throw new HttpResponseException(response()->json($response, 400));
    }
}
