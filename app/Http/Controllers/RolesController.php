<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller
{
    function __construct()
    {
        $this->middleware('type:admin', ['except' => ['show']]);
    }

    public function index()
    {
        $roles = Role::all();
        return response()->json(['roles' => $roles], 200);
    }

    public function show()
    {
        $role = Auth::user()->role->descricao;
        return response()->json(["role" => $role], 200);
    }
}
