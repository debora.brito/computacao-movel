<?php

namespace App\Http\Controllers;

use App\Exceptions\InternalErrorException;
use App\Models\Customer as ModelsCustomer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomersController extends Controller
{
    function __construct()
    {
        $this->middleware('type:admin');
    }

    public function index()
    {
        $customers = ModelsCustomer::paginate(15);
        return response()->json(['customers' => $customers], 200);
    }

    public function store(Request $request)
    {
        try {
            Validator::make($request->input(), [
                'name' => 'required',
            ], [
                'name.required' => 'O valor é obrigatório',
            ]);
            $customer = new ModelsCustomer();
            $customer->name = $request->name;
            $customer->surname = $request->surname;
            $customer->save();
            return response()->json(['customer' => $customer], 201);
        } catch (\Throwable $th) {
            return response()->json(['Errors' => [['server' => 'Falha ao salvar cliente.']]], 500);
        }
    }

    public function show($id)
    {
        try {
            $customer = ModelsCustomer::findOrFail($id);
            return response()->json(['customer' => $customer], 200);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                return response()->json(['Errors' => [['server' => 'Cliente não encontrado.']]], 404);
            }
            throw new InternalErrorException();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            Validator::make($request->input(), [
                'name' => 'required',
            ], [
                'name.required' => 'O valor é obrigatório',
            ]);
            $customer = ModelsCustomer::findOrFail($id);
            $customer->name = $request->name;
            $customer->surname = $request->surname;
            $customer->save();
            return response()->json(['customer' => $customer], 201);
        } catch (\Throwable $th) {
            return response()->json(['Errors' => [['server' => 'Falha ao salvar cliente.']]], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $customer = ModelsCustomer::findOrFail($id)->delete();
            return response()->json(['customer' => 'excluído com sucesso']);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                return response()->json(['Errors' => [['server' => 'Cliente não encontrado.']]], 404);
            }
            throw new InternalErrorException();
        }
    }
}
