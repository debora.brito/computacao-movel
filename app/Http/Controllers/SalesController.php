<?php

namespace App\Http\Controllers;

use App\Exceptions\InternalErrorException;
use App\Models\Sale;
use App\Models\Tip;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SalesController extends Controller
{

    public function index()
    {
        $sales = Sale::paginate(15);
        return response()->json(['sales' => $sales], 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Validator::make($request->input(), [
                'total' => 'required|numeric',
                'tip' => 'required|numeric',
                'products' => 'required',
            ], [
                'total.required' => 'O valor total é obrigatório',
                'products.required' => 'Os produtos são obrigatórios',
                'total.numeric' => 'O valor total deve ser númerico',
                'tip.required' => 'o valor da gorjeta é obrigatório',
                'tip.numeric' => 'o valor da gorjeta deve ser um número',
            ]);

            $sale = new Sale();
            $sale->total = $request->total;
            $sale->tip = $request->tip;
            $sale->waiter_id = $request->waiter_id;
            $sale->customer_id = $request->customer_id;
            $sale->products = json_encode($request->product);
            $sale->save();
            return response()->json(['sale' => $sale], 201);
        } catch (\Throwable $th) {
            return response()->json(['Errors' => [['server' => 'Falha ao salvar venda.']]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $sale = Sale::findOrFail($id);
            return response()->json(['sale' => $sale], 200);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                return response()->json(['Errors' => [['server' => 'Cliente não encontrado.']]], 404);
            }
            throw new InternalErrorException();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Validator::make($request->input(), [
                'total' => 'required|numeric',
                'tip' => 'required|numeric',
                'products' => 'required',
            ], [
                'total.required' => 'O valor total é obrigatório',
                'total.numeric' => 'O valor total deve ser númerico',
                'products.required' => 'Os produtos são obrigatórios',
                'tip.required' => 'o valor da gorjeta é obrigatório',
                'tip.numeric' => 'o valor da gorjeta deve ser um número',
            ]);
            $sale = Sale::findOrFail($id);
            $sale->total = $request->total;
            $sale->products = json_encode($request->product);
            $sale->save();
            return response()->json(['sale' => $sale], 201);
        } catch (\Throwable $th) {
            return response()->json(['Errors' => [['server' => 'Falha ao salvar cliente.']]], 500);
        }
    }
}
