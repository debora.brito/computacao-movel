<?php

namespace App\Http\Controllers\Authentication;

use App\Exceptions\InternalErrorException;
use App\Exceptions\User\UserNotFoundException;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login()
    {
        try {
            $validator = Validator::make(["email" => request()->email], [
                "email" => "required|email"
            ], [
                'email.required' => 'O e-mail é obrigatório',
                'email.email' => 'Insira um e-mail válido',
            ]);

            if ($validator->fails()) {
                return response()->json(['Errors' => [$validator->errors()]], 400);
            }
            if (!Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                return response(['Errors' => [['auth' => 'E-mail ou Senha Incorretos']]], 401);
            }
            $id   = auth()->user()->id;
            $user  = User::find($id);
            $token = $user->createToken('token')->plainTextToken;
            return response(['token' => $token, "user" => $user], 201);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new UserNotFoundException();
            }
            throw new InternalErrorException();
        }
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            'message' => 'deslogado'
        ]);
    }
}
