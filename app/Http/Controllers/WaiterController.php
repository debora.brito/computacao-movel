<?php

namespace App\Http\Controllers;

use App\Exceptions\InternalErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\Waiter\StoreWaiterException;
use App\Exceptions\Waiter\WaiterNotFoundException;
use App\Http\Requests\User\StoreRequest as UserStoreRequest;
use App\Models\User;
use App\Models\Waiter;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WaiterController extends Controller
{
    function __construct()
    {
        $this->middleware('type:admin');
    }

    public function index()
    {
        $waiters = Waiter::all();
        return response()->json(['waiters' => $waiters], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        try {
            $waiter = new Waiter();
            $user = new User();
            $waiter->name = $request->name;
            $waiter->surname = $request->surname;
            $waiter->photo = $request->photo;
            $waiter->save();

            $user->name = $waiter->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role_id = 2;
            $user->save();
            event(new Registered($user));
            return response()->json(['waiter' => $waiter, 'user' => $user], 201);
        } catch (\Throwable $th) {
            throw new StoreWaiterException();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $waiter = Waiter::findOrFail($id);
            return response()->json(['product' => $waiter], 200);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new WaiterNotFoundException();
            }
            throw new InternalErrorException();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Validator::make($request->input(), [
                'name' => 'required',
            ], [
                'name.required' => 'O nome é obrigatório',
            ]);
            $waiter = Waiter::findOrFail($id);
            $waiter->name = $request->name;
            $waiter->surname = $request->surname;
            $waiter->photo = $request->photo;
            $waiter->save();
            return response()->json(['waiter' => $waiter], 201);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new WaiterNotFoundException();
            }
            throw new InternalErrorException();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $waiter = Waiter::findOrFail($id)->delete();
            return response()->json(['waiter' => 'excluído com sucesso']);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new WaiterNotFoundException();
            }
            throw new InternalErrorException();
        }
    }
}
