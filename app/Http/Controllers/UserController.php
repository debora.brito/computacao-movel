<?php

namespace App\Http\Controllers;

use App\Exceptions\InternalErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\User\StoreUserException;
use App\Exceptions\User\UserNotFoundException;
use App\Http\Requests\User\StoreRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        $users = User::paginate(6);
        return response()->json(["count" => $users->count(), "users" => $users], 200);
    }

    public function store(StoreRequest $request)
    {
        try {
            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role_id = 1;
            $user->save();
            event(new Registered($user));
            $token = $user->createToken('token')->plainTextToken;
            return response()->json(['user' => $user, 'token' => $token], 201);
        } catch (\Throwable $th) {
            dd($th);
            throw new StoreUserException();
        }
    }

    public function show(){
        $user = Auth::user();
        return response()->json(['user' => $user]);
    }

    public function update(StoreRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return response()->json(['user' => $user], 201);
        } catch (\Throwable $th) {
            throw new StoreUserException();
        }
    }

    public function destroy($id)
    {
        try {
            if (Auth::id() == $id) {
                return response()->json(['error' => 'proibido excluir sua própria conta'], 403);
            }
            $user = User::findOrFail($id)->delete();
            return response()->json(['sucess' => 'Usuário excluído com sucesso'], 200);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new UserNotFoundException();
            }
            throw new InternalErrorException();
        }
    }
}
