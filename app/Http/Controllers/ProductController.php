<?php

namespace App\Http\Controllers;

use App\Exceptions\InternalErrorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\Product\ProductNotFoundException;
use App\Exceptions\Product\StoreProductException;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    function __construct()
    {
        $this->middleware('type:admin', ['except' => ['show', 'index']]);
    }

    public function index()
    {
        $products = Product::paginate(15);
        return response()->json(['products' => $products], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->input(), [
                'name' => 'required',
                'price' => 'required|numeric'
            ], [
                'name.required' => 'O nome é obrigatório',
                'price.required' => 'O preço é obrigatório',
                'price.numeric' => 'O valor deve ser um número'
            ]);
            if ($validator->fails()) {
                return response()->json(['Errors' => [$validator->errors()]], 400);
            }
            $product = new Product();
            $product->name = $request->name;
            $product->price = $request->price;
            $product->save();
            return response()->json(['product' => $product], 201);
        } catch (\Throwable $th) {
            throw new StoreProductException();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);
            return response()->json(['product' => $product], 200);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new ProductNotFoundException();
            }
            throw new InternalErrorException();
        }
    }


    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'name' => 'required',
                'price' => 'required|numeric'
            ], [
                'name.required' => 'O nome é obrigatório',
                'price.required' => 'O preço é obrigatório',
                'price.numeric' => 'O valor deve ser um número'
            ]);
            if ($validator->fails()) {
                return response()->json(['Errors' => [$validator->errors()]], 400);
            }
            $product = Product::findOrFail($id);
            $product->update($request->all());
            return response()->json(['product' => $product], 201);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new ProductNotFoundException();
            }
            throw new InternalErrorException();
        }
    }

    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id)->delete();
            return response()->json(['product' => 'excluído com sucesso']);
        } catch (\Throwable $th) {
            if ($th instanceof ModelNotFoundException) {
                throw new ProductNotFoundException();
            }
            throw new InternalErrorException();
        }
    }
}
