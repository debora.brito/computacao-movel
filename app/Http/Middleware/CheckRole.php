<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);
        foreach ($roles as $role) {
            if (auth()->user()->role->descricao === $role) {
                return $next($request);
            }
        }
        return response()->json(["Errors" => [["user" => "Usuário precisa de autorização"]]], 403);
    }
}
